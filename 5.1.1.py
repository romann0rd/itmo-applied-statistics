from math import *


def sortKey(a):
	return a[0]


print('Введите 3 числа в формате \u03B7 = a * \u03B5^x + b')
x, a, b = int(input('Введите x: ')), int(input('Введите a: ')), int(input('Введите b: '))

table = []
for i in range(4):
	table.append([a * int(input('Введите ' + str(i + 1) + '-е значение \u03B5: ')) ** x + b, float(input('Введите вероятность (P) ' + str(i + 1) + '-го \u03B5: '))])

indexes = []
for i in table:
	for j in range(table.index(i) + 1, len(table)):
		if i[0] == table[j][0]:
			i[1] += table[j][1]
			indexes.append(j)
	for f in indexes:
		table.pop(f)
	indexes.clear()
table.sort(key = sortKey)

print('{:<10}'.format('\u03B7'), end='')
for i in table:
	print(f'|{i[0]:<11}', end='')
print()
print('-' * 54)
print('{:<10}'.format('P'), end='')
for i in table:
	print(f'|{i[1]:<11}', end='')